#Desafio Capgemini

## Para Rodar o Projeto clone o repositório e importe na sua ide de preferência como maven project

### após rodar o projeto espere o sistema incializar o bd de exemplo e aparecere mensagem de inicialização 
```
Inserindo Pagamentos ... 
Pagamentos Salvos!
Started DesafioApplication in 21.307 seconds (JVM running for 23.466)
```

#### vá no seu navegador de preferência e abra o [swagger](http://localhost:8080/swagger-ui.html)

 expanda o **pagamento-controller** e vá no primeiro endpoint **GET** aperte em:
 ```
  Try It Out!
```

Confira o array de json e os seus respectivos percentuais mensais.

o registro de id **7** por exemplo vem com **100%** pois é o único pagamento do 
>  CPF 725.435.790-01 em fevereiro de 2021

```
  {
    "id": 7,
    "nomeDestinatario": "João Palhares",
    "cpfdestinatario": "887.128.750-90",
    "dataPagamento": "2021-02-05T02:34:14",
    "cpforigem": "725.435.790-01",
    "descricao": "Ajuda de Custo",
    "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
    "valor": 300,
    "instituicaoBancaria": 237,
    "percentagemMensal": 100
  }
```

vamos inserir um novo pagamento de R$300 identico para testarmos o **C** do CRUD e 
o recalculo automático da **percentagemMensal**   
> json para inserção no endpont **POST**
>> mandamos **sem** id e **sem** o percentagemMensal
```
  {
    "nomeDestinatario": "João Palhares",
    "cpfdestinatario": "887.128.750-90",
    "dataPagamento": "2021-02-06T02:34:14",
    "cpforigem": "725.435.790-01",
    "descricao": "Ajuda de Custo",
    "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
    "valor": 300,
    "instituicaoBancaria": 237
  }
```

> json esperado pelo retorno do post
>>  **com** id e **sem** percentagemMensal

```
{
  "id": 8,
  "nomeDestinatario": "João Palhares",
  "instituicaoBancaria": 237,
  "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
  "valor": 300,
  "dataPagamento": "2021-02-06T02:34:14",
  "descricao": "Ajuda de Custo",
  "cpforigem": "725.435.790-01",
  "cpfdestinatario": "887.128.750-90"
}
```

> vamos no endpoint g **GET** /**id** e conferir os novos percentuais
>> id 7
```
{
  "id": 7,
  "nomeDestinatario": "João Palhares",
  "cpforigem": "725.435.790-01",
  "valor": 300,
  "chavePIX": "deff940f-69a8-40a9-af04-fd0d21b213a3",
  "cpfdestinatario": "887.128.750-90",
  "descricao": "Ajuda de Custo",
  "dataPagamento": "2021-02-05T02:34:14",
  "percentagemMensal": 50,
  "instituicaoBancaria": 237
}
```

>>id 8
```
{
  "id": 8,
  "nomeDestinatario": "João Palhares",
  "cpforigem": "725.435.790-01",
  "valor": 300,
  "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
  "cpfdestinatario": "887.128.750-90",
  "descricao": "Ajuda de Custo",
  "dataPagamento": "2021-02-06T02:34:14",
  "percentagemMensal": 50,
  "instituicaoBancaria": 237
}
```

 Agora Vamos testar o **U** e atualizar o registro de id **8** trocando a descrição
> isso é possível a usar o endpoint **PUT** 
>> lembre que o ID **8** tem que ser passável no path e no body neste caso 
```
{
  "id": 8,
  "nomeDestinatario": "João Palhares",
  "cpforigem": "725.435.790-01",
  "valor": 300,
  "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
  "cpfdestinatario": "887.128.750-90",
  "descricao": "Ajuda de Custo 2",
  "dataPagamento": "2021-02-06T02:34:14",
  "percentagemMensal": 50,
  "instituicaoBancaria": 237
}
```

>> json de retorno
```
{
  "id": 8,
  "nomeDestinatario": "João Palhares",
  "instituicaoBancaria": 237,
  "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
  "valor": 300,
  "dataPagamento": "2021-02-06T02:34:14",
  "descricao": "Ajuda de Custo 2",
  "cpforigem": "725.435.790-01",
  "cpfdestinatario": "887.128.750-90"
}
```

>> colocar um id no path diferente do body ocasionará em erro:
>>> Response Code: **422**
```
 objeto informado não coincide com o parâmetro informado
```
 
 Agora Vamos testar o **D** e apagando o registro **7** 
 > isso é possível a usar o endpoint **DELETE**
 > não precisa de body somente o id **7** no path do endpoint
>
>>>> Response Code: **200** confere sucesso na remoção

Por fim vamos ao primeiro endpoint de listar todos os pagamentos
> devemos conferir a **ausencia** do registro com id 7
>> devemos conferir o regitro de id 8 com percentualMensal de **100%**
>>> devemos conferir o registro de id 8 com descrição **"Ajuda de Custo 2"** 
 

```
... ( outros ids )
,
  {
    "id": 6,
    "nomeDestinatario": "Almir dutra",
    "cpforigem": "725.435.790-01",
    "valor": 400,
    "chavePIX": "faeb9131-1f91-4c67-85cf-6f84d8e57eea",
    "cpfdestinatario": "529.210.860-00",
    "descricao": "Ajuda de Custo",
    "dataPagamento": "2021-01-05T19:02:22",
    "percentagemMensal": 100,
    "instituicaoBancaria": 104
  },
  {
    "id": 8,
    "nomeDestinatario": "João Palhares",
    "cpforigem": "725.435.790-01",
    "valor": 300,
    "chavePIX": "68f11586-4b11-40bb-990a-9530377ea740",
    "cpfdestinatario": "887.128.750-90",
    "descricao": "Ajuda de Custo 2",
    "dataPagamento": "2021-02-06T02:34:14",
    "percentagemMensal": 100,
    "instituicaoBancaria": 237
  }
```


## Fique a Vontade Para Fazer Quaisquer alterações

#Muito Obrigado!
 > [Linkedin](https://www.linkedin.com/in/rodrigo-lima-barros-48aa233a/)