package rodrigao.dev.model;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(name="cpf_origem", nullable = false, updatable = false)
    private String CPFOrigem;

    @Column(name="cpf_destinatario", nullable = false, updatable = false)
    private String CPFDestinatario;

    @Column(name="nome_destinatario", nullable = false)
    private String nomeDestinatario;

    @Column(name="instituicao_bancaria", nullable = false, updatable = false)
    private Short instituicaoBancaria;

    @Column(columnDefinition = "uuid", name="chave_pix", updatable = false)
    private UUID chavePIX;

    @Column(name="valor", updatable = false)
    private BigDecimal valor;

    @Column(name="data_pagamento", updatable = false)
    private LocalDateTime  dataPagamento;

    @Column(name="descricao")
    private String descricao;
}
