package rodrigao.dev.utils;

import java.util.stream.StreamSupport;

public class IterableUtils {
    public static long size(Iterable iterable){
        return StreamSupport.stream(iterable.spliterator(), false).count();
    }

    public static  boolean isEmpty(Iterable iterable){
        return size(iterable) == 0L;
    }
}
