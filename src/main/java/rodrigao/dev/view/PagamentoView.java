package rodrigao.dev.view;

import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public interface PagamentoView {
    @Value("#{target.id}")
    Long getId();

    @Value("#{target.cpf_origem}")
    String getCPFOrigem();

    @Value("#{target.cpf_destinatario}")
    String getCPFDestinatario();

    @Value("#{target.nome_destinatario}")
    String getNomeDestinatario();

    @Value("#{target.instituicao_bancaria}")
    Short getinstituicaoBancaria();

    @Value("#{target.chave_pix}")
    String getChavePIX();

    @Value("#{target.valor}")
    BigDecimal getValor();

    @Value("#{target.data_pagamento}")
    LocalDateTime getDataPagamento();

    @Value("#{target.descricao}")
    String getDescricao();

    @Value("#{target.percentagem_mensal}")
    BigDecimal getPercentagemMensal();
}
