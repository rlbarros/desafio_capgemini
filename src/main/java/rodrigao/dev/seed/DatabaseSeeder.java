package rodrigao.dev.seed;

import org.apache.tomcat.jni.Local;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import rodrigao.dev.model.Pagamento;
import rodrigao.dev.repository.PagamentoRepository;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class DatabaseSeeder {

    @Autowired
    PagamentoRepository pagamentoRepository;


    Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedPagamentos();
    }

    private class Pessoa{
        String nome;
        String CPF;
        Short instituicaoBancaria;
        UUID chavePIX;
    }

    public void seedPagamentos(){

        String cpfa = "689.751.480-25";
        String cpfb = "725.435.790-01";

        Pessoa pessoaC = new Pessoa();
        pessoaC.nome = "Almir dutra";
        pessoaC.CPF = "529.210.860-00";
        pessoaC.instituicaoBancaria = 104;
        pessoaC.chavePIX = UUID.randomUUID();

        Pessoa pessoaD = new Pessoa();
        pessoaD.nome = "João Palhares";
        pessoaD.CPF = "887.128.750-90";
        pessoaD.instituicaoBancaria = 237;
        pessoaD.chavePIX = UUID.randomUUID();

        Pessoa pessoaE = new Pessoa();
        pessoaE.nome = "Renato Garcia";
        pessoaE.CPF = "018.264.420-20";
        pessoaE.instituicaoBancaria = 001;
        pessoaE.chavePIX = UUID.randomUUID();

        BigDecimal v1 = BigDecimal.valueOf(10L);
        BigDecimal v2 = BigDecimal.valueOf(5L);
        BigDecimal v3 = BigDecimal.valueOf(8L);
        BigDecimal v4 = BigDecimal.valueOf(18L);
        BigDecimal v5 = BigDecimal.valueOf(40L);
        BigDecimal v6 = BigDecimal.valueOf(400L);
        BigDecimal v7 = BigDecimal.valueOf(300L);

        LocalDateTime d1 = LocalDateTime.of(2021,1,5,13,25,47);
        LocalDateTime d2 = LocalDateTime.of(2021,1,12,8,14,41);
        LocalDateTime d3 = LocalDateTime.of(2021,1,15,11,10,38);
        LocalDateTime d4 = LocalDateTime.of(2021,2,5,15,31,5);
        LocalDateTime d5 = LocalDateTime.of(2021,2,15,22,55,15);
        LocalDateTime d6 = LocalDateTime.of(2021,1,5,19,2,22);
        LocalDateTime d7 = LocalDateTime.of(2021,2,5,2,34,14);

        Pagamento p1 = new Pagamento(0L, cpfa, pessoaC.CPF, pessoaC.nome, pessoaC.instituicaoBancaria, pessoaC.chavePIX, v1, d1, "Lanche");
        Pagamento p2 = new Pagamento(0L, cpfa, pessoaD.CPF, pessoaD.nome, pessoaD.instituicaoBancaria, pessoaD.chavePIX, v2, d2, "Reparo");
        Pagamento p3 = new Pagamento(0L, cpfa, pessoaE.CPF, pessoaE.nome, pessoaE.instituicaoBancaria, pessoaE.chavePIX, v3, d3, "Passeio");
        Pagamento p4 = new Pagamento(0L, cpfa, pessoaC.CPF, pessoaC.nome, pessoaC.instituicaoBancaria, pessoaC.chavePIX, v4, d4, "Roupa");
        Pagamento p5 = new Pagamento(0L, cpfa, pessoaD.CPF, pessoaD.nome, pessoaD.instituicaoBancaria, pessoaD.chavePIX, v5, d5, "Mensalidade");
        Pagamento p6 = new Pagamento(0L, cpfb, pessoaC.CPF, pessoaC.nome, pessoaC.instituicaoBancaria, pessoaC.chavePIX, v6, d6, "Ajuda de Custo");
        Pagamento p7 = new Pagamento(0L, cpfb, pessoaD.CPF, pessoaD.nome, pessoaD.instituicaoBancaria, pessoaD.chavePIX, v7, d7, "Ajuda de Custo");


        logger.info("Inserindo Pagamentos ... ");
        this.pagamentoRepository.saveAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7));
        logger.info("Pagamentos Salvos!");

    }


}
