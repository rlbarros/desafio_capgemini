package rodrigao.dev.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rodrigao.dev.model.Pagamento;
import rodrigao.dev.service.PagamentoService;
import rodrigao.dev.utils.IterableUtils;
import rodrigao.dev.view.PagamentoView;

import java.util.Optional;

@RestController
@RequestMapping("/api/pagamentos")
@Api(value = "API de Interação com os Pagamentos PIX")
@CrossOrigin(origins = "*")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @GetMapping
    @ResponseBody
    @ApiOperation(value="Retorna todos os pagamentos da base de dados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listagem retornada com sucesso", response = PagamentoView[].class),
            @ApiResponse(code = 404, message =  "Nenhum resultado encontrado"),
            @ApiResponse(code = 500, message =  "Erro do Servidor")
    })
    public ResponseEntity<Iterable<PagamentoView>> listar(){
        try{
            Iterable<PagamentoView> pagamentos = this.pagamentoService.listar();
            return new ResponseEntity<>(pagamentos, IterableUtils.isEmpty(pagamentos) ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }catch (Exception ex) {
            return  new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ApiOperation(value="Retorna o pagamentos da base de dados que tem o id informado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pagamento retornado com sucesso", response = PagamentoView[].class),
            @ApiResponse(code = 404, message =  "Nenhum resultado encontrado"),
            @ApiResponse(code = 500, message =  "Erro do Servidor")
    })
    public ResponseEntity<PagamentoView> encontrarPorId(@PathVariable(value="id", required = true) Long id){
        try{
            Optional<PagamentoView> optionalPagamentoView = this.pagamentoService.encontrar(id);
            if(optionalPagamentoView.isPresent()){
                return new ResponseEntity<>(optionalPagamentoView.get(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        }catch (Exception ex) {
            return  new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity salvar(Pagamento pagamento){
        Optional<Pagamento> optionalPagamentoSalvo = this.pagamentoService.salvar(pagamento);
        if(optionalPagamentoSalvo.isPresent()){
            return new ResponseEntity<>(optionalPagamentoSalvo.get(), HttpStatus.OK);
        }else{
            throw new RuntimeException("Não houve confirmação no banco de dados do registro");
        }
    }

    @PostMapping
    @ResponseBody
    @ApiOperation(value="Insere um pagamento")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pagamento inserido com sucesso.", response = Pagamento.class),
            @ApiResponse(code = 422, message =  "O pagamento informado é inválido", response = String.class)
    })
    public ResponseEntity inserir(@RequestBody Pagamento pagamento){
        try{
            return salvar(pagamento);
        }catch (Exception ex) {
            return  new ResponseEntity<>(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    @PutMapping("/{id}")
    @ResponseBody
    @ApiOperation(value="Atualiza um pagamento")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pagamento atualizado com sucesso.", response = Pagamento.class),
            @ApiResponse(code = 422, message =  "O pagamento informado é inválido", response = String.class)
    })
    public ResponseEntity atualizar(@PathVariable(value="id", required = true) Long id, @RequestBody Pagamento pagamento){
        try{
            if(!id.equals(pagamento.getId())){
                throw new RuntimeException("O objeto informado não coincide com o parâmetro informado");
            }
            return salvar(pagamento);
        }catch (Exception ex) {
            return  new ResponseEntity<>(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="Remove um pagamento")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pagamento removido com sucesso."),
            @ApiResponse(code = 422, message =  "O código de pagamento informado é inválido")
    })
    public ResponseEntity deletar(@PathVariable(value="id", required = true) Long id){
        try{
            this.pagamentoService.apagar(id);
            return  new ResponseEntity(HttpStatus.OK);
        }catch (Exception ex) {
            return  new ResponseEntity<>(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
