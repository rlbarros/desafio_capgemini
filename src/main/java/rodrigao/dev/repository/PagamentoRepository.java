package rodrigao.dev.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import rodrigao.dev.model.Pagamento;
import rodrigao.dev.view.PagamentoView;

import java.util.Optional;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    final String SQL_BASE =  " with x as "
            + " ( "
            + " 	select "
            + " 	p.*, "
            + " 	p.cpf_origem || '|' || to_char(p.data_pagamento, 'YYYY-MM') as chaveCPFAnoMes "
            + " 	from pagamento p "
            + " ), y as ( "
            + " 	select x.chaveCPFAnoMes, sum(valor) as valorTotalCPFAnoMes from x "
            + " 	group by x.chaveCPFAnoMes "
            + " 	 "
            + " ), z as ( "
            + " 	select "
            + " 		x.id, "
            + " 		x.cpf_origem, "
            + " 		x.cpf_destinatario, "
            + " 		x.nome_destinatario, "
            + " 		x.instituicao_bancaria, "
            + " 		CAST(x.chave_pix as text) as chave_pix, "
            + " 		x.valor, "
            + " 		x.data_pagamento, "
            + " 		x.descricao, "
            + " 		CAST((x.valor/y.valorTotalCPFAnoMes*100) AS numeric(7,4)) as percentagem_mensal "
            + " 	from x "
            + " 	inner join y on x.chaveCPFAnoMes = y.chaveCPFAnoMes "
            + " ) "
            + " select * from z";

    @Query(value = SQL_BASE, nativeQuery = true)
    Iterable<PagamentoView> listarPagamentosComPercentualMensal();

    @Query(value = SQL_BASE + " where z.id = :id", nativeQuery = true)
    Optional<PagamentoView> encontrarPagamentoComPercentualMensalPorId(@Param("id") Long id);

}
