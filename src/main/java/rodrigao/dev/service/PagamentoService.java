package rodrigao.dev.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rodrigao.dev.model.Pagamento;
import rodrigao.dev.repository.PagamentoRepository;
import rodrigao.dev.view.PagamentoView;

import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Iterable<PagamentoView> listar(){
        return this.pagamentoRepository.listarPagamentosComPercentualMensal();
    }

    public Optional<PagamentoView> encontrar(Long id){
        return this.pagamentoRepository.encontrarPagamentoComPercentualMensalPorId(id);
    }

    public Optional<Pagamento> salvar(Pagamento pagamento){
        return Optional.of(this.pagamentoRepository.save(pagamento));
    }

    public void apagar(Long id){
        this.pagamentoRepository.deleteById(id);
    }
}
